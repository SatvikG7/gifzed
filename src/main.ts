import LogRocket from "logrocket";
import "./style.css";
import { Endpoint, Search } from "./vite-env";

LogRocket.init('1hzx7l/gifzed');

const app = document.querySelector<HTMLDivElement>("#app")!;
const Endpoint: Endpoint = "https://g.tenor.com/v1/search?q=";
const key = "CEZFBVC28TO2";

app.innerHTML = `
    <form id="form">
        <input type="search" name="search" id="search" placeholder="Search for GIFS and GET GIFZED!" >
    </form>
`;
const gifsDiv = document.createElement("div");
const form = document.querySelector<HTMLFormElement>("#form");
const searchBox: any = document.querySelector<HTMLInputElement>("#search");
app.appendChild(gifsDiv);

form?.addEventListener("submit", (e) => {
    e.preventDefault();

    gifsDiv.innerHTML = " ";

    if (searchBox.value) {
        let Search: Search = searchBox.value;
        fetchGifs(Search).then((data) => {
            gifsDiv.innerHTML = " ";

            data.results.forEach((e: any) => {
                // console.log(e);
                const { gif, nanogif } = JSON.parse(JSON.stringify(e.media[0]));
                let img: HTMLImageElement = document.createElement("img");
                img.className = "pic";
                img.src = nanogif.url;
                img.ondblclick = () => {
                    window.open(`${gif.url}`);
                };
                gifsDiv.className = "gifsDiv";
                gifsDiv.appendChild(img);
            });
        });
    } else {
        gifsDiv.innerHTML = " ";
    }
});

async function fetchGifs(query: String) {
    const res = await fetch(`${Endpoint.toString() + query}&key=${key}`);
    const data = await res.json();
    return data;
}
