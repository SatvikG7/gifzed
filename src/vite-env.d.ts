/// <reference types="vite/client" />

/**
 * Endpoint of tenor api
 */
export type Endpoint = "https://g.tenor.com/v1/search?q=" | String;

/**
 * Query to search
 */
export type Search = String;

export function fetchGifs(query: String): Promise<JSON>;
